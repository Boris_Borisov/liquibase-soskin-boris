--liquibase formatted sql

--changeset bugarchev:01
CREATE TABLE CARDS(
                      id INT PRIMARY KEY,
                      name VARCHAR(30) NOT NULL
)


--changeset bugarchev:02
CREATE TABLE ACCOUNTS(
                      id INT PRIMARY KEY,
                      name VARCHAR(12) NOT NULL,
                      type VARCHAR(25)
)